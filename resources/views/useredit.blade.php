@extends('layouts.app')

@section('content')
<form action=" {{ route('user.update', ['id' => $user->id]) }}" method="POST">
    @csrf
    @method('PUT')
    <label for="name">Nombre: </label>
    <input id='name' name='name' type="text" value='{{ $user->name }}'>
    <label for="email">Email: </label>
    <input id='email' name='email' type="email" value='{{ $user->email }}'>
    <button type="submit">guardar</button>
</form>
@endsection