<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UsersContrroller@index')->name('users.index');
Route::delete('/users/{user}', 'UsersController@destroy')->name('user.destroy');

Route::put('/users/{user}','UsersController@update')->name('user.update');
Route::get('/users/{user}', 'UsersController@show')->name('user.show');
Route::get('/users/{user}/edit', 'UsersController@edit')->name('user.edit');